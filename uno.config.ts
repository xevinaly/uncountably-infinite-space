import { defineConfig, presetIcons, presetTypography, presetUno, presetWebFonts } from 'unocss';
import { presetScrollbar } from 'unocss-preset-scrollbar';

export default defineConfig({
	presets: [
		presetUno(),
		presetIcons(),
		presetWebFonts({
			fonts: {
				source: ['Source Code Pro', 'sans-serif']
			}
		}),
		presetTypography(),
		presetScrollbar()
	],
	theme: {
		colors: {
			grey: '#EEEBE8',
			smoke: '#F3F0ED',
			flare: '#FBF8F5',
			ruby: '#FF3E00',
			gray: '#303030',
			soot: '#2B2B2B',
			eclipse: '#232323',
			terminal: '#39FF14',
			horizon: 'rgba(251,114,0,0.8)',
			stop: '#B81D13',
			yield: '#EFB700',
			go: '#008450'
		}
	}
});
