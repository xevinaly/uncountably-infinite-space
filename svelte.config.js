import adapter from '@sveltejs/adapter-node';

const config = {
	kit: {
		adapter: adapter({
			precompress: true
		})
	}
};

export default config;
