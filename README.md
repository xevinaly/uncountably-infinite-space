# Uncountably Infinite Space

In 2019, I read a blog post by an old friend that set me on the path to creating my own website. I've had the skills for a basic website for years, but had never committed to creating my own. I'd never known what to put, or what to use. This is an attempt to get myself on track with that.

I then dropped the project for a year or two and have returned in 2024, hoping to rebuild what I had using the latest tech tools.

## Contributing

This is going to be a personal project, but I wanted to make it available to anyone that wanted to view the source for it, either for their own site, or just as a reference.

I'm using Svelte5 with as my dev environment/framework and have a [live site](http://uncountablyinfinite.space/) deployed to a Noder server on Digital Ocean.

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
yarn run dev

# or start the server and open the app in a new browser tab
yarn run dev -- --open
```

## Building

To create a production version of your app:

```bash
yarn run build
```

You can preview the production build with `yarn run preview`.
